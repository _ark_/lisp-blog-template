(defpackage :recurse.blog
  (:use :cl)
  (:export :start-server :stop-server :regenerate-blog))
(in-package :recurse.blog)
(ql:quickload '(:hunchentoot :coleslaw))

(load "settings.lisp")

;; serve files in
(defvar *folder-handler*
  (hunchentoot:create-folder-dispatcher-and-handler "/" *web-path*))

;; Map "/" to /index.html
(hunchentoot:define-easy-handler (site-root
                                  :uri
                                  (lambda (request)
                                    (let ((uri (hunchentoot:request-uri request)))
                                      (equalp uri "/"))))
    ()
  (hunchentoot:redirect "/index.html"))

(setf hunchentoot:*dispatch-table*
      (append hunchentoot:*dispatch-table*
              ;; folder-handler last so custom handlers can run first
              (list *folder-handler*)))

(defvar *http-handler*
  (make-instance 'hunchentoot:easy-acceptor
                 :name 'http
                 :document-root *web-path*
                 :port 80))

(defvar *https-handler* nil)

(defun start-server ()
  (uiop:run-program (format nil "mkdir -p ~A" *web-dir*)
                    :ignore-error-status T)
  (hunchentoot:start *http-handler*)
  (when *enable-ssl*
    (setf *https-handler*
          (make-instance 'hunchentoot:easy-ssl-acceptor
                         :name 'ssl
                         :document-root *web-path*
                         :ssl-privatekey-file *ssl-privatekey-path*
                         :ssl-certificate-file *ssl-certificate-path*
                         :port 443))
    (hunchentoot:define-easy-handler (redir-to-ssl :uri (lambda (req) (declare (ignore req)) t) :acceptor-names '(http)) ()
      (hunchentoot:redirect "/" :protocol :https))
    (hunchentoot:start *https-handler*)))

(defun stop-server ()
  (hunchentoot:stop *http-handler*)
  (when *enable-ssl*
    (hunchentoot:stop *https-handler*)
    (hunchentoot:define-easy-handler (redir-to-ssl :uri (lambda (req) (declare (ignore req)) t) :acceptor-names '()) ())))

(defun regenerate-blog ()
  (uiop:run-program (format nil "rm -rf ~A/*" *web-dir*)
                    :ignore-error-status T)
  (uiop:run-program (format nil "mkdir -p ~A" *web-dir*)
                    :ignore-error-status T)
  (coleslaw:main *blog-source-dir*)
  (flet ((deploy (file destination)
           (uiop:run-program (format nil "rsync ~A/~A ~A/"
                                     *blog-source-dir* file destination)
                             :output *standard-output*
                             :error-output *error-output*)))
    (loop :for (file destination) :on *additional-files-to-deploy*
       :by #'cddr :while destination :do
         (deploy file destination))))
